<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function ($browser)  {
            $browser->visit('/login')
                ->type('email', 'mail174@mail.com')
                ->type('password', '1234')
                ->press('Login')
                ->assertPathIs('/home');
        });
    }
}
