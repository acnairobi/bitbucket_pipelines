<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PipelineSuccess extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return SlackMessage
     */

    public function toSlack($notifiable)
    {
        return (new SlackMessage)
            ->success()
            ->from(env('APP_NAME'))
            ->content('Bitbucket pipeline was successful')
            ->attachment(function ($attachment) {
                $attachment->title(env('APP_NAME'). ' Pipeline')
                    ->fields([
                        'Name' => 'Bitbucket Pipeline' ,
                        'Status' => 'Success',
                        'Message' => 'Cheers'
                    ]);
            });
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
