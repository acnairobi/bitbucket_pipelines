<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Notification;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/slack/notify', function () {
    return Notification::route('slack', env('SLACK_WEBHOOK_URL'))->notify(new \App\Notifications\PipelineSuccess());
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
